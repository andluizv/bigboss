# Sobre o projeto:
Este projeto foi realizado em grupo com: Iasmin(https://gitlab.com/oliviasmim), João(https://gitlab.com/joaogarbini), Jonatan(https://gitlab.com/JonatanPSilva) e Lorena(https://gitlab.com/lorenabelo).
Tinha por objetivo desenvolver uma aplicação completa em React e usar o json-server(fake api) para fazer requisições e persistir dados.
Desenvolvemos uma aplicação voltada para gestão de projetos e clientes de programadores, onde são fornecidas ferramentas para gestão da carteira de clientes, nos projetos (em andamento ou não) e da parte financeira.


## Link da versão final:
https://bigboss.vercel.app

## Minha participação:
Participei principalmente na montagem e integração da nossa fake-api, personalização de componentes do material(inputs principalmente), a geração dos relatórios para download via excel, e estruturação das páginas de clientes, serviços e contratos.

#### Maiores desafios
Acredito que o sistema do json-server devido as limitações foi bem complicado de encaixar com as nossas necessidades no front, diversas requisições não puderam ser tratadas no lado do servidor e foi necessário vários ajustes na parte do front-end.

### Tecnologias e bibliotecas utilizadas:
* ReactJs;
* Styled Components;
* Material-Ui;
* Chart-Js;
* Axios;
* Json-Server;
* Excel-Js;
* Js-File-Download;
* Remask;




